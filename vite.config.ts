import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src')
    }
  },
  plugins: [vue()],
  base: `/${process.env.NODE_ENV === "production" ? process.env.CI_PROJECT_NAME + '/' : ''}`, //process.env.CI_PROJECT_NAME
  build: { outDir: "public" },
  publicDir: "static"
})
