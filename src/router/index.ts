import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/views/Home.vue';
import Option from "@/views/Option.vue";

const routes = [
    {
        path: "/",
        redirect: "/home",
    },
    {
        path: "/home",
        name: 'Home',
        component: Home,
    },
    {
        path:"/option",
        name: "Option",
        component: Option,
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router