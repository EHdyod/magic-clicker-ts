import { MutationTree } from 'vuex';
import { State } from './state';
import { PentagramMutationTypes as MutationTypes } from './mutation-types';
import Seal from "@/store/types/Seal";
import Slot from "@/store/types/Slot";

export type Mutations<S = State> = {
    [MutationTypes.INIT_PENTAGRAM](state: S, name: string|null): void;
    [MutationTypes.ADD_SEAL_PENTA](state: S, payload : {seal: Seal, ind: number}): void;
    [MutationTypes.REM_SEAL_PENTA](state: S, index: number): void;
}

export const mutations: MutationTree<State> & Mutations = {
    [MutationTypes.INIT_PENTAGRAM](state: State, name: string|null = null) {
        if(name){
            state.name = name;
        }
        state.nbEmbedded = 0;
        state.totalEnergy = 0;

        state.slots.map((s: Slot) =>{
           s.setSeal = Seal.nullSeal;
        });
        state.slots[0].linkedSlot = [state.slots[2], state.slots[3]];
        state.slots[1].linkedSlot = [state.slots[3], state.slots[4]];
        state.slots[2].linkedSlot = [state.slots[0], state.slots[4]];
        state.slots[3].linkedSlot = [state.slots[0], state.slots[1]];
        state.slots[4].linkedSlot = [state.slots[1], state.slots[2]];

        savePentagram(state);
    },
    [MutationTypes.ADD_SEAL_PENTA](state: State, {seal, ind}) {
        state.slots[ind].setSeal = seal;
        state.nbEmbedded++;
        state.totalEnergy = state.slots.reduce((a,b) => a+b.energy, 0);
        savePentagram(state);
    },
    [MutationTypes.REM_SEAL_PENTA](state: State, index: number) {
        state.slots[index].setSealEmpty();
        state.nbEmbedded--;
        state.totalEnergy = state.slots.reduce((a,b) => a+b.energy, 0);
        savePentagram(state);
    },
};

function savePentagram(state: State) {
    const slotsArray = state.slots.map((s: Slot) =>{
       return s.serialize;
    });
    const str = JSON.stringify({
        name : state.name,
        nbEmbedded: state.nbEmbedded,
        nbMaxEmbedded: state.nbMaxEmbedded,
        totalEnergy: state.totalEnergy,
        slots: slotsArray
    });

    window.localStorage.setItem("pentagram", str);
}