import { ActionTree, ActionContext } from 'vuex';
import { RootState } from '@/store';
import { State } from './state';
import { PentagramActionTypes as ActionTypes} from './action-types';
import { PentagramMutationTypes as MutationTypes } from './mutation-types';
import Seal from "@/store/types/Seal";
import {elements} from "@/store/types/Elements";
import Slot from "@/store/types/Slot";

export interface Actions {
    [ActionTypes.INIT_PENTAGRAM]({ commit }: ActionContext<State, RootState>): void;
    [ActionTypes.ADD_SEAL_PENTA]({ commit }: ActionContext<State, RootState>, payload: {seal: Seal, index: number}): void;
    [ActionTypes.REM_SEAL_PENTA]({ commit }: ActionContext<State, RootState>, index: number): void;
    [ActionTypes.RESET_PENTAGRAM]({ commit }: ActionContext<State, RootState>): void;
    [ActionTypes.SET_PENTAGRAM]({ state, commit}: ActionContext<State, RootState>, raw: string): void;
}

export const actions: ActionTree<State, RootState> & Actions = {
    [ActionTypes.INIT_PENTAGRAM]({ commit }){
        const name = "Astral Pentagram";
        commit(MutationTypes.INIT_PENTAGRAM, name);
    },
    [ActionTypes.ADD_SEAL_PENTA]({ commit }, {seal, index}){
        commit(MutationTypes.ADD_SEAL_PENTA, {seal, ind:index});
    },
    [ActionTypes.REM_SEAL_PENTA]({ commit }, index){
        commit(MutationTypes.REM_SEAL_PENTA, index);
    },
    [ActionTypes.RESET_PENTAGRAM]({ commit }){
        commit(MutationTypes.INIT_PENTAGRAM);
    },
    [ActionTypes.SET_PENTAGRAM]({ state, commit}, raw){
        const json = JSON.parse(raw);
        commit(MutationTypes.INIT_PENTAGRAM);

        json.slots.forEach((s : string, index: number) =>{
            const rawSeal = JSON.parse(JSON.parse(s)["seal"]);
            if(rawSeal.element !== "Void"){
                const seal = new Seal(rawSeal.name, elements[rawSeal.element.toLowerCase()], rawSeal.strength);
                commit(MutationTypes.ADD_SEAL_PENTA, {seal: seal, ind: index});
            }
        });
        let energy = 0;
        state.slots.forEach((s: Slot) => {
            energy += s.energy;
        });
        state.totalEnergy = energy;
    }
};
