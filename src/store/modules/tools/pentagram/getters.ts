import { GetterTree } from 'vuex';
import { RootState } from '@/store';
import { State } from './state';
import Slot from "@/store/types/Slot";

export type Getters = {
    getPentagramName(state: State): string,
    getNbEmbedded(state: State): number,
    getNbMaxEmbedded(state: State): number,
    getPentagramEnergy(state: State): number,
    getPentagramSlots(state: State): Slot[],
    getPentagramEmptySlots(state: State): Slot[],
}

export const getters: GetterTree<State, RootState> & Getters = {
    getPentagramName(state: State): string{
        return state.name;
    },
    getNbEmbedded(state: State): number{
        return state.nbEmbedded;
    },
    getNbMaxEmbedded(state: State): number{
        return state.nbMaxEmbedded;
    },
    getPentagramEnergy(state: State): number{
        return state.totalEnergy;
    },
    getPentagramSlots(state: State): Slot[]{
        return state.slots.map((s: Slot) =>{
            return s
        });
    },
    getPentagramEmptySlots(state: State): Slot[]{
        return state.slots.filter((s: Slot) =>{
            return s.isEmpty;
        });
    }
};