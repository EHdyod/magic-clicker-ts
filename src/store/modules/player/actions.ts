import { ActionTree, ActionContext } from 'vuex';
import { RootState } from '@/store';
import { State } from './state';
import { PlayerActionTypes as ActionTypes} from './action-types';
import { PlayerMutationTypes as MutationTypes } from './mutation-types';
import Seal from "@/store/types/Seal";
import {Element, elements} from "@/store/types/Elements";

export interface Actions {
    [ActionTypes.INIT_PLAYER]({ commit }: ActionContext<State, RootState>, name: string): void;
    [ActionTypes.RENAME]({ commit }: ActionContext<State, RootState>, name: string): void;
    [ActionTypes.EARN_ENERGY]({ commit }: ActionContext<State, RootState>, amount: number): void;
    [ActionTypes.BUY_SEAL]({ commit }: ActionContext<State, RootState>, price: number): void;
    [ActionTypes.REM_SEAL_BAG]({ commit }: ActionContext<State, RootState>, name: string): void;
    [ActionTypes.RESET_PLAYER]({ commit }: ActionContext<State, RootState>): void;
    [ActionTypes.SET_PLAYER]({ commit }: ActionContext<State, RootState>, raw: string): void;
    [ActionTypes.ADD_SEAL_BAG]({ commit }: ActionContext<State, RootState>, seal: Seal): void;
}

export const actions: ActionTree<State, RootState> & Actions = {
    [ActionTypes.INIT_PLAYER]({ commit }, name = "Player"){
        commit(MutationTypes.INIT_PLAYER, name);
    },
    [ActionTypes.EARN_ENERGY]({ commit }, amount){
        commit(MutationTypes.ADD_ENERGY, amount);
    },
    [ActionTypes.BUY_SEAL]({ commit }, price){
        commit(MutationTypes.REM_ENERGY, price);
        const elemNames = Object.keys(elements);
        const element: Element = elements[elemNames[Math.floor(Math.random() * (elemNames.length-1))]];
        const seal = new Seal(`lorem ${element.name}`, element,2);
        commit(MutationTypes.ADD_SEAL_BAG, seal);
    },
    [ActionTypes.REM_SEAL_BAG]({state, commit }, name){
        const seals = state.seals;
        const seal = seals.filter((s: Seal) =>{
            return s.name === name;
        })[0];
        const index = seals.indexOf(seal);
        commit(MutationTypes.REM_SEAL_BAG, index);
    },
    [ActionTypes.RESET_PLAYER]({ state, commit }){
        const name = state.name;
        commit(MutationTypes.INIT_PLAYER, name);
    },
    [ActionTypes.RENAME]({ commit}, name){
        commit(MutationTypes.SET_NAME, name);
    },
    [ActionTypes.SET_PLAYER]({ state, commit }, raw){
        const json = JSON.parse(raw);
        const seals: Seal[] = json.seals.map((s: string) =>{
            const slot = JSON.parse(s);
            const elem = elements[slot.element.toLowerCase()];
            return new Seal(slot.name, elem, slot.strength);
        });
        state.name = json.name;
        state.energy = json.energy;
        state.seals = seals;
    },
    [ActionTypes.ADD_SEAL_BAG]({ commit }, seal){
        commit(MutationTypes.ADD_SEAL_BAG, seal);
    }
};
