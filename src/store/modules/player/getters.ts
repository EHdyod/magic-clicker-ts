import { GetterTree } from 'vuex';
import { RootState } from '@/store';
import { State } from './state';
import Seal from "@/store/types/Seal";

export type Getters = {
    getPlayerName(state: State): string,
    getPlayerEnergy(state: State): number,
    getPlayerSeals(state: State): Seal[],
}

export const getters: GetterTree<State, RootState> & Getters = {
    getPlayerName(state: State): string {
        return state.name;
    },
    getPlayerEnergy(state: State): number {
        return state.energy;
    },
    getPlayerSeals(state: State): Seal[] {
        return state.seals;
    },
};