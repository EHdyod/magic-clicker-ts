import { MutationTree } from 'vuex';
import { State } from './state';
import { PlayerMutationTypes as MutationTypes } from './mutation-types';
import Seal from "@/store/types/Seal";

export type Mutations<S = State> = {
    [MutationTypes.INIT_PLAYER](state: S, name: string): void;
    [MutationTypes.SET_NAME](state: S, name: string): void;
    [MutationTypes.ADD_ENERGY](state: S, amount: number): void;
    [MutationTypes.REM_ENERGY](state: S, amount: number): void;
    [MutationTypes.RESET_ENERGY](state: S): void;
    [MutationTypes.ADD_SEAL_BAG](state: S, seal: Seal): void;
    [MutationTypes.REM_SEAL_BAG](state: S, index: number): void;
    [MutationTypes.EMPTY_SEALS](state: S): void;
}

export const mutations: MutationTree<State> & Mutations = {
    [MutationTypes.INIT_PLAYER](state: State, name: string) {
        state.name = name;
        state.energy = 0;
        state.seals = new Array<Seal>();
        savePlayer(state);
        console.log("initialisation complète!");
    },
    [MutationTypes.SET_NAME](state: State, name: string){
        state.name = name;
        savePlayer(state);
    },
    [MutationTypes.ADD_ENERGY](state: State, amount: number) {
        state.energy += amount ;
        savePlayer(state);
    },
    [MutationTypes.REM_ENERGY](state: State, amount: number) {
        state.energy -= amount;
        savePlayer(state);
    },
    [MutationTypes.ADD_SEAL_BAG](state: State, seal: Seal) {
        state.seals.push(seal);
        savePlayer(state);
    },
    [MutationTypes.REM_SEAL_BAG](state: State, ind: number) {
        state.seals = state.seals.filter((s: Seal, index: number) =>{
            return ind !== index;
        });
        savePlayer(state);
    },
    [MutationTypes.RESET_ENERGY](state: State) {
        state.energy = 0;
        savePlayer(state);
    },
    [MutationTypes.EMPTY_SEALS](state: State) {
        state.seals = new Array<Seal>();
        savePlayer(state);
    }
};

function savePlayer(state: State) {
    const seals = state.seals.map((s:Seal) =>{
        return s.serialize();
    });
    const str = JSON.stringify({
        name: state.name,
        energy: state.energy,
        seals: seals
    });
    window.localStorage.setItem("player", str);
}