import {Element, elements} from "@/store/types/Elements";

export class Sign {
    private readonly _name: string;
    private readonly _element: Element;
    private _opposite: Sign | null;
    static nullSign: Sign = new Sign("void", elements.void);

    constructor(name: string, element: Element) {
        this._name = name;
        this._element = element;
        this._opposite = null;
    }

    set setOpposite(sign: Sign) {
        this._opposite = sign;
    }

    get name() : string{
        return this._name;
    }

    get element() : string {
        return this._element.name;
    }

    get opposite() : string | null {
        if(this._opposite){
            return this._opposite?.name;
        }else{
            return null;
        }
    }
}

const waters: Sign[] = [
    new Sign("water1", elements.water),
    new Sign("water2", elements.water),
    new Sign("water3", elements.water)
];

const fires: Sign[] = [
    new Sign("fire1", elements.fire),
    new Sign("fire2", elements.fire),
    new Sign("fire3", elements.fire)
];

const earths: Sign[] = [
    new Sign("earth1", elements.earth),
    new Sign("earth2", elements.earth),
    new Sign("earth3", elements.earth)
];

const winds: Sign[] = [
    new Sign("wind1", elements.wind),
    new Sign("wind2", elements.wind),
    new Sign("wind3", elements.wind)
];

waters.forEach((s: Sign, index: number) =>{
   s.setOpposite = fires[index];
});
fires.forEach((s: Sign, index: number) =>{
   s.setOpposite = waters[index];
});
earths.forEach((s: Sign, index: number) =>{
    s.setOpposite = winds[index];
});
winds.forEach((s: Sign, index: number) =>{
    s.setOpposite = earths[index];
});

interface Signs {
    [key: string]: Sign[];
}

export const signs: Signs = {
    "water": waters,
    "fire": fires,
    "earth": earths,
    "wind": winds,
};