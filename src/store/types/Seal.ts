import {Element, elements} from "@/store/types/Elements";
import {Sign, signs} from "@/store/types/Sign";

export default class Seal {
    readonly _name: string;
    readonly _strength: number;
    readonly _element : Element;
    readonly _sign : Sign;
    static readonly nullSeal = new Seal("null", elements.void, 0);

    constructor(name: string, elem: Element, strength: number) {
        this._name = name;
        this._element = elem;
        this._strength = strength;
        if(elem.name === "Void"){
            this._sign = Sign.nullSign;
        }else{
            const signs_elem = signs[elem.name.toLowerCase()];
            this._sign = signs_elem[Math.floor(Math.random() * (signs_elem.length-1))];
        }
    }

    get name(): string{
        return this._name;
    }
    get strength(): number {
        return this._strength;
    }
    get element(): string {
        return this._element.name;
    }
    get elementalAffinity(): string | null{
        return this._element.affinity;
    }
    get elementalNeutrality(): string | null{
        return this._element.neutral;
    }
    get elementRejection(): string | null{
        return this._element.rejection;
    }
    get sign(): string{
        return this._sign.name;
    }
    get signOpposite(): string{
        return this._sign.opposite!;
    }

    serialize(): string{
        const simplify = {
            name: this.name,
            element: this.element,
            strength: this.strength
        };
        return JSON.stringify(simplify);
    }
    copy(): Seal{
        return new Seal(this._name, this._element, this._strength);
    }
}