import Seal from "@/store/types/Seal";

const affinityMultiplier: number = 1.5;
const neutralMultiplier: number = 1;
const rejectionMultiplier: number = 0.5;
const sameMultiplier: number = 0.8;

export default class Slot {
    private _harmony: number;
    private _linkedSlot: Slot[];
    private _seal: Seal;
    readonly nbLinkedSlot: number;

    constructor(nbLinkedSlot: number, linkedSlots: Slot[] = []) {
        this._harmony = 1;
        this._linkedSlot = linkedSlots;
        this.nbLinkedSlot = nbLinkedSlot;
        this._seal = Seal.nullSeal;

    }

    calculateHarmony(): void {
        if(this._seal.element !== Seal.nullSeal.element){
            const element = this._seal.element;
            const rejection = this._seal.elementRejection;
            const neutral = this._seal.elementalNeutrality;
            const affinity = this._seal.elementalAffinity;
            const multiplier: number[] = [];
            this._linkedSlot.forEach((slot: Slot)=>{
                const linkedElement = slot._seal?.element;
                if (linkedElement === element){
                    multiplier.push(sameMultiplier);
                }else if (linkedElement === affinity){
                    multiplier.push(affinityMultiplier);
                }else if (linkedElement === neutral){
                    multiplier.push(neutralMultiplier);
                }else if(linkedElement === rejection){
                    multiplier.push(rejectionMultiplier);
                }
            });
            this._harmony = multiplier.reduce((a, b)=> a*b, 1);
        }else{
            this._harmony = 1;
        }
    }

    get isEmpty(): boolean {
        return this._seal.element === Seal.nullSeal.element;
    }

    set linkedSlot(newSlots: Slot[]) {
        if(newSlots.length === this.nbLinkedSlot) this._linkedSlot = newSlots;
        this.calculateHarmony();
    }

    set setSeal(seal: Seal) {
        this._seal = seal;
        this.calculateHarmony();
        this._linkedSlot.forEach((s: Slot) =>{
            s.calculateHarmony();
        })
    }

    setSealEmpty(): void {
        this._seal = Seal.nullSeal;
        this.calculateHarmony();
        this._linkedSlot.forEach((s: Slot) =>{
            s.calculateHarmony();
        })
    }

    get seal(): Seal {
        return this._seal;
    }

    get energy(): number {
        return this._seal.strength * this._harmony;
    }

    get serialize(): string {
        const res = {
            seal : this.seal?.serialize()
        };
        return JSON.stringify(res);
    }
}