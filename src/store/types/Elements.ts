export class Element {
    private _name: string;
    private _affinity: Element | null;
    private _neutral: Element | null;
    private _rejection: Element | null;

    constructor(name: string) {
        this._name = name;
        this._affinity = null;
        this._neutral = null;
        this._rejection = null;
    }

    set name(name: string) {
        this._name = name;
    }
    set setAffinity(elem: Element){
        if(!this._affinity) this._affinity = elem;
    }
    set setRejection(elem: Element){
        if(!this._rejection) this._rejection = elem;
    }
    set setNeutral(elem: Element){
        if(!this._neutral) this._neutral = elem;
    }

    get name(): string {
        return this._name;
    }
    get affinity(): string | null{
        if(this._affinity){
            return this._affinity.name;
        }else{
            return null;
        }
    }
    get neutral(): string | null {
        if(this._neutral){
            return this._neutral.name;
        }else{
            return null;
        }
    }
    get rejection(): string | null{
        if(this._rejection){
            return this._rejection.name;
        }else{
            return null;
        }
    }
}

const Water = new Element("Water");
const Fire = new Element("Fire");
const Earth = new Element("Earth");
const Wind = new Element("Wind");

Water.setAffinity = Wind;
Water.setNeutral = Earth;
Water.setRejection = Fire;
Fire.setAffinity = Earth;
Fire.setNeutral = Wind;
Fire.setRejection = Water;
Earth.setAffinity = Fire;
Earth.setNeutral = Water;
Earth.setRejection = Wind;
Wind.setAffinity = Water;
Wind.setNeutral = Fire;
Wind.setRejection = Earth;

const Void = new Element("Void");

export const elements: {[key:string]: Element} = {
    water: Water,
    fire: Fire,
    earth: Earth,
    wind: Wind,
    void: Void
};

