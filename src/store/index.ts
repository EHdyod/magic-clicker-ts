import { createStore } from 'vuex';

import { store as player, PlayerStore, State as PlayerState } from '@/store/modules/player';
import { store as pentagram, PentagramStore, State as PentagramState } from "@/store/modules/tools/pentagram";

export type RootState = {
  playerState: PlayerState,
  pentagramState: PentagramState
};

export type Store = PlayerStore<Pick<RootState, 'playerState'>>
    & PentagramStore<Pick<RootState, 'pentagramState'>>;

export default createStore({
  modules: {
    player,
    pentagram
  }
})
